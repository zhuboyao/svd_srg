//
//  SVD_Base.c
//  
//
//  Created by Boyao Zhu on 10/29/19.
//

#include "SVD_Base.h"


//**********************************************************************
//*** initialize SVD structure  ****************************************
//**********************************************************************
//
void SVD_Init(SVD_Struct *pSVD, int iMax, int nComponent)
{
    int p00, p02, p22, pp;
    int i;
    pSVD->iMax = iMax;
    pSVD->nComponent = nComponent;
    
    // count single and couple partial wave
    //int sgCount=0;
    //int cpCount=0;
    pSVD->sgCount = 0;
    pSVD->cpCount = 0;
    
    for(p00=0; p00<=PWB.pMax; p00++){
        
        if((PWB.L[p00]==PWB.J[p00] && PWB.LL[p00]==PWB.J[p00]) ||
        (PWB.L[p00]==1 && PWB.LL[p00]==1 && PWB.J[p00]==0)) {
            pSVD->sgCount += 1;
        }
    }
    
    for(p00=0; p00<=PWB.pMax; p00++){
        
        if(PWB.L[p00]==PWB.J[p00]-1 && PWB.LL[p00]==PWB.J[p00]-1) {
            p02 = p22 = -1;
            for(pp=0; pp<=PWB.pMax; pp++) {
                if((PWB.J[pp]==PWB.J[p00]) && (PWB.L[pp]==PWB.J[p00]-1) && (PWB.LL[pp]==PWB.J[p00]+1) &&
                   (PWB.S[pp]==PWB.S[p00]) && (PWB.T[pp]==PWB.T[p00]) && (PWB.MT[pp]==PWB.MT[p00])) {
                    p02 = pp;
                }
                if((PWB.J[pp]==PWB.J[p00]) && (PWB.L[pp]==PWB.J[p00]+1) && (PWB.LL[pp]==PWB.J[p00]+1) &&
                   (PWB.S[pp]==PWB.S[p00]) && (PWB.T[pp]==PWB.T[p00]) && (PWB.MT[pp]==PWB.MT[p00])) {
                    p22 = pp;
                }
            }
            if(p02>0 && p22>0) {
                pSVD->cpCount += 1;
            }
            else {
                pSVD->sgCount += 1;
            }
        }
    }
    
    //pSVD->sgCount = sgCount;
    //pSVD->cpCount = cpCount;
    
    pSVD->sgPc = (int *) malloc(sizeof(int)*(pSVD->sgCount));
    
    // allocate memeory for single-partial-wave singular vectors and singular values
    int Dim1 = pSVD->sgCount;
    int Dim2 = pSVD->iMax+1;
    int Dim3 = pSVD->nComponent;

    pSVD->sgU           = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
    for (i=0; i<Dim1; i++)
        pSVD->sgU[i]    = (gsl_matrix *) gsl_matrix_alloc(Dim2, Dim3);
    
    pSVD->sgV           = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
    for (i=0; i<Dim1; i++)
        pSVD->sgV[i]    = (gsl_matrix *) gsl_matrix_alloc(Dim2, Dim3);
    
    pSVD->sgS           = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
    for (i=0; i<Dim1; i++)
        pSVD->sgS[i]    = (gsl_matrix *) gsl_matrix_alloc(Dim3, Dim3);

    
    if(!pSVD->sgU || !pSVD->sgU[0]){
        UTI_ErrorMessage("SVD_Init", "could not allocate SVD structure for single partial wave left singular vectors!\n");
        exit(-1);
    }
    
    if(!pSVD->sgV || !pSVD->sgV[0]){
        UTI_ErrorMessage("SVD_Init", "could not allocate SVD structure for single partial wave right singular vectors!\n");
        exit(-1);
    }
    
    if(!pSVD->sgS || !pSVD->sgS[0]){
        UTI_ErrorMessage("SVD_Init", "could not allocate SVD structure for single partial wave singular values!\n");
        exit(-1);
    }
    
    
    if (pSVD->cpCount != 0){
        // allocate memeory for coupled-partial-wave singular vectors and singular values
        Dim1 = pSVD->cpCount;
        Dim2 = (pSVD->iMax+1)*2;
        Dim3 = pSVD->nComponent*2;
    
        pSVD->cpU           = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
        for (i=0; i<Dim1; i++)
            pSVD->cpU[i]    = (gsl_matrix *) gsl_matrix_alloc(Dim2, Dim3);
    
        pSVD->cpV           = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
        for (i=0; i<Dim1; i++)
            pSVD->cpV[i]    = (gsl_matrix *) gsl_matrix_alloc(Dim2, Dim3);
    
        pSVD->cpS           = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
        for (i=0; i<Dim1; i++)
            pSVD->cpS[i]    = (gsl_matrix *) gsl_matrix_alloc(Dim3, Dim3);

    
        if(!pSVD->cpU || !pSVD->cpU[0]){
            UTI_ErrorMessage("SVD_Init", "could not allocate SVD structure for coupled partial wave left singular vectors!\n");
            exit(-1);
        }
            
        if(!pSVD->cpV || !pSVD->cpV[0]){
            UTI_ErrorMessage("SVD_Init", "could not allocate SVD structure for coupled partial wave right singular vectors!\n");
            exit(-1);
        }
            
        if(!pSVD->cpS || !pSVD->cpS[0]){
            UTI_ErrorMessage("SVD_Init", "could not allocate SVD structure for coupled partial wave singular values!\n");
            exit(-1);
        }
    
        pSVD->cpPc = (int *) malloc(sizeof(int)*(pSVD->cpCount)*3);
        pSVD->cpT = (gsl_matrix *) gsl_matrix_alloc((iMax+1)*2,(iMax+1)*2);
        
        if (!pSVD->cpT){
            UTI_ErrorMessage("SVD_Init", "could not allocate cpT operators");
            exit(-1);
        }
        
        pSVD->cpEta = (gsl_matrix *) gsl_matrix_alloc((iMax+1)*2,(iMax+1)*2);
        
        if (!pSVD->cpEta){
            UTI_ErrorMessage("SVD_Init", "could not allocate eta operators");
            exit(-1);
        }
        
        pSVD->cpWeight = (gsl_matrix *) gsl_matrix_alloc((iMax+1)*2, (iMax+1)*2);
        
        if (!pSVD->cpWeight){
            UTI_ErrorMessage("SVD_Init", "could not allocate conversion matrix");
        }
        
    }
    //allocate memory for const T matrix for SP and TT for CP and set values from pMEQ
    pSVD->sgT = (gsl_matrix *) gsl_matrix_alloc(iMax+1,iMax+1);
    
    if (!pSVD->sgT){
        UTI_ErrorMessage("SVD_Init", "could not allocate sgT operators");
        exit(-1);
    }
    
    //allocate memroy for Eta operators
    pSVD->sgEta = (gsl_matrix *) gsl_matrix_alloc(iMax+1,iMax+1);
    
    if (!pSVD->sgEta){
        UTI_ErrorMessage("SVD_Init", "could not allocate eta operators");
        exit(-1);
    }
    
    pSVD->sgWeight = (gsl_matrix *) gsl_matrix_alloc(iMax+1, iMax+1);
    
    if (!pSVD->sgWeight){
        UTI_ErrorMessage("SVD_Init", "could not allocate conversion matrix");
    }
    
}

//**********************************************************************
//*** Singular Value Decomposition for non-coupled channel ***********************
//**********************************************************************
//
void SVD_SinglePW(MEQ_Struct *pMEQ, SVD_Struct *pSVD, int p00, int count)
{
    int iDim, nComponent;
    int i, j;
    
    iDim = pSVD->iMax+1;
    nComponent = pSVD->nComponent;

    //take interactions from pMEQ
    gsl_matrix *m = gsl_matrix_alloc(iDim, iDim);
    if (!m){
        UTI_ErrorMessage("SVD_SinglePW", "could not allocate for Interactions");
        exit(-1);
    }
    
    for (i = 0; i < iDim; i++){
        for (j = 0; j < iDim; j++){
            gsl_matrix_set(m, i, j, pMEQ->me[p00][i][j]);
        }
    }

    gsl_matrix_scale(m,1.0/INVMU);
    gsl_matrix_mul_elements(m, pSVD->sgWeight);
    
    //allocate memory for full singular vectors and singular values
    gsl_matrix *U = gsl_matrix_alloc(iDim,iDim);
    gsl_vector *S = gsl_vector_alloc(iDim);
    gsl_matrix *V = gsl_matrix_alloc(iDim,iDim);
    gsl_vector *work = gsl_vector_alloc(iDim);
    
    //check memory allocation
    if (!U || !S || !V || !work){
        UTI_ErrorMessage("SVD_SinglePW", "could not allocate for U, S, V, work for Decomposition");
        exit(-1);
    }
    //do SVD
    gsl_matrix_memcpy(U,m);
    gsl_linalg_SV_decomp(U,V,S,work);
    
    //get lower rank of singular vectors
    gsl_matrix_view U_low = gsl_matrix_submatrix(U,0,0,iDim,nComponent);
    gsl_matrix_view V_low = gsl_matrix_submatrix(V,0,0,iDim,nComponent);
    
    gsl_matrix_memcpy(pSVD->sgU[count],&U_low.matrix);
    gsl_matrix_memcpy(pSVD->sgV[count],&V_low.matrix);
    
    //get lower rank of singular values
    gsl_matrix_set_zero(pSVD->sgS[count]);
    for (i = 0; i < nComponent; i++){
        gsl_matrix_set(pSVD->sgS[count], i, i, gsl_vector_get(S,i));
    }
    
    gsl_vector_free(work);
    gsl_vector_free(S);
    gsl_matrix_free(V);
    gsl_matrix_free(U);
    gsl_matrix_free(m);
    
}
 
//**********************************************************************
//*** Singular Value Decomposition for coupled channel ***********************
//**********************************************************************
//
void SVD_CouplePW(MEQ_Struct *pMEQ, SVD_Struct *pSVD, int p00, int p02, int p22, int count)
{
    int iDim, nComponent, iDim2;
    int i, j;
    iDim = pSVD->iMax+1;
    nComponent = pSVD->nComponent*2;
    iDim2 = iDim*2;
    
    gsl_matrix *m = gsl_matrix_alloc(iDim2, iDim2);
    if (!m){
        UTI_ErrorMessage("SVD_CouplePW", "could not allocate for Interactions");
        exit(-1);
    }
    
    //take and form interactions from pMEQ
    for (i = 0; i < iDim; i++){
        for (j = 0; j < iDim; j++){
            gsl_matrix_set(m, i, j, pMEQ->me[p00][i][j]);
            gsl_matrix_set(m, i+iDim, j+iDim, pMEQ->me[p22][i][j]);
            gsl_matrix_set(m, i+iDim, j, pMEQ->me[p02][j][i]);
            gsl_matrix_set(m, i, j+iDim, pMEQ->me[p02][i][j]);
        }
    }
    
    gsl_matrix_scale(m,1.0/INVMU);
    gsl_matrix_mul_elements(m, pSVD->cpWeight);
    
    //allocate memory for full singular vectors and singular values
    gsl_matrix *U = gsl_matrix_alloc(iDim2,iDim2);
    gsl_vector *S = gsl_vector_alloc(iDim2);
    gsl_matrix *V = gsl_matrix_alloc(iDim2,iDim2);
    gsl_vector *work = gsl_vector_alloc(iDim2);
        
    
    //check memory allocation
    if (!U || !S || !V || !work){
        UTI_ErrorMessage("SVD_CouplePW", "could not allocate for U, S, V, work for Decomposition");
        exit(-1);
    }
    
    //do SVD
    gsl_matrix_memcpy(U,m);
    gsl_linalg_SV_decomp(U,V,S,work);

    
    //get lower rank of singular vectors
    gsl_matrix_view U_low = gsl_matrix_submatrix(U,0,0,iDim2,nComponent);
    gsl_matrix_view V_low = gsl_matrix_submatrix(V,0,0,iDim2,nComponent);
        
    gsl_matrix_memcpy(pSVD->cpU[count],&U_low.matrix);
    gsl_matrix_memcpy(pSVD->cpV[count],&V_low.matrix);
        
    //get lower rank of singular values
    gsl_matrix_set_zero(pSVD->cpS[count]);
    for (i = 0; i < nComponent; i++){
        gsl_matrix_set(pSVD->cpS[count], i, i, gsl_vector_get(S,i));
    }
    
    // free space
    gsl_vector_free(work);
    gsl_vector_free(S);
    gsl_matrix_free(V);
    gsl_matrix_free(U);
    gsl_matrix_free(m);
   
}

                
void SVD_MEQ(MEQ_Struct *pMEQ, SVD_Struct *pSVD)
{
    int p00,p02,p22,pp;
    int i, j, k;
    
    UTI_InfoMessage("SVD_allMEQ", "[");
    int iMax = pSVD->iMax;
    
    gsl_matrix_set_zero(pSVD->sgT);
    for (k = 0; k < iMax+1; k++){
        gsl_matrix_set(pSVD->sgT, k, k, gsl_pow_2(pMEQ->q[k]));
    }
    
    double qiqj;
    for (i = 0; i < iMax+1; i++){
        for (j = 0; j < iMax+1; j++){
            qiqj = pMEQ->q[i]*pMEQ->q[j];
            if(qiqj == 0.0) qiqj=1e-14;
            gsl_matrix_set(pSVD->sgWeight, i, j, qiqj*sqrt(pMEQ->qWeight[i]*pMEQ->qWeight[j]));
        }
    }
    
    if (pSVD->cpCount != 0){
        gsl_matrix_set_zero(pSVD->cpT);
        for (k = 0; k < (iMax+1)*2; k++){
            gsl_matrix_set(pSVD->cpT, k, k, gsl_pow_2(pMEQ->q[k%(iMax+1)]));
        }
        
        for (i = 0; i < (iMax+1)*2; i++){
            for (j = 0; j < (iMax+1)*2; j++){
                qiqj = (pMEQ->q[i%(iMax+1)])*(pMEQ->q[j%(iMax+1)]);
                if(qiqj == 0) qiqj=1e-14;
                gsl_matrix_set(pSVD->cpWeight, i, j, qiqj*sqrt((pMEQ->qWeight[i%(iMax+1)])*(pMEQ->qWeight[j%(iMax+1)])));
            }
        }
    }
    
    
    //*** loop over single partial waves
    int sgCount = 0;
    i = 0;
    for (p00=0; p00<=PWB.pMax; p00++){
        if((PWB.L[p00]==PWB.J[p00] && PWB.LL[p00]==PWB.J[p00]) ||
        (PWB.L[p00]==1 && PWB.LL[p00]==1 && PWB.J[p00]==0)) {
            UTI_InfoMessage0("+");
            SVD_SinglePW(pMEQ,pSVD,p00,sgCount);
            pSVD->sgPc[i] = p00;
            i += 1;
            sgCount += 1;
        }
    }
    
    int cpCount = 0;
    j = 0;
    for(p00=0; p00<=PWB.pMax; p00++) {
        if(PWB.L[p00]==PWB.J[p00]-1 && PWB.LL[p00]==PWB.J[p00]-1) {
            // find other partial waves
            p02 = p22 = -1;
            for (pp=0; pp<PWB.pMax; pp++){
                if((PWB.J[pp]==PWB.J[p00]) && (PWB.L[pp]==PWB.J[p00]-1) && (PWB.LL[pp]==PWB.J[p00]+1) &&
                   (PWB.S[pp]==PWB.S[p00]) && (PWB.T[pp]==PWB.T[p00]) && (PWB.MT[pp]==PWB.MT[p00])) {
                    p02 = pp;
                }
                if((PWB.J[pp]==PWB.J[p00]) && (PWB.L[pp]==PWB.J[p00]+1) && (PWB.LL[pp]==PWB.J[p00]+1) &&
                   (PWB.S[pp]==PWB.S[p00]) && (PWB.T[pp]==PWB.T[p00]) && (PWB.MT[pp]==PWB.MT[p00])) {
                    p22 = pp;
                }
            }
            if (p02>0 && p22>0) {
                UTI_InfoMessage0("#");
                SVD_CouplePW(pMEQ,pSVD,p00,p02,p22,cpCount);
                pSVD->cpPc[j]   = p00;
                pSVD->cpPc[j+1] = p02;
                pSVD->cpPc[j+2] = p22;
                j += 3;
                cpCount += 1;
            }
            else {
                UTI_InfoMessage0("+");
                SVD_SinglePW(pMEQ,pSVD,p00,sgCount);
                pSVD->sgPc[i] = p00;
                i += 1;
                sgCount += 1;
            }
        }
    }
    UTI_InfoMessage0("]\n\n");
}

    
void SVD_WriteFile(MEQ_Struct *pMEQ, SVD_Struct *pSVD, char *MEID)
{
    FILE   *fp;
    char   filename[BUFSIZE];
    int    p, i, j;
    
    //*** loop over single partial waves
    for(p=0; p<pSVD->sgCount; p++) {
        
        //*** compose file name
        if(PWB.NoCD) {
            sprintf(filename, "%s/%s_JLLST%i%i%i%i%i.meq", PAR.MEDir, MEID, PWB.J[pSVD->sgPc[p]], PWB.L[pSVD->sgPc[p]], PWB.LL[pSVD->sgPc[p]], PWB.S[pSVD->sgPc[p]], PWB.T[pSVD->sgPc[p]]);
        }
        else {
            sprintf(filename, "%s/%s_JLLSTMT%i%i%i%i%i%+i.meq", PAR.MEDir, MEID, PWB.J[pSVD->sgPc[p]], PWB.L[pSVD->sgPc[p]], PWB.LL[pSVD->sgPc[p]], PWB.S[pSVD->sgPc[p]], PWB.T[pSVD->sgPc[p]], PWB.MT[pSVD->sgPc[p]]);
        }
        
        //*** open file
        UTI_InfoMessage("MEQ_WriteFile", "writing '%s'... ", filename);
        fp = fopen(filename,"w");
        
        fprintf(fp, "# %s/%s \n", PROG_ID, PROG_VERSION);
        fprintf(fp, "# \n");
        fprintf(fp, "# momentum space matrix elements \n");
        fprintf(fp, "# partial wave J=%i, L=%i/%i, S=%i, T=%i, MT=%i \n", PWB.J[pSVD->sgPc[p]], PWB.L[pSVD->sgPc[p]], PWB.LL[pSVD->sgPc[p]], PWB.S[pSVD->sgPc[p]], PWB.T[pSVD->sgPc[p]], PWB.MT[pSVD->sgPc[p]]);
        fprintf(fp, "# \n");
        fprintf(fp, "# momentum grid [fm^-1]\n");
        for(i=0; i<=pSVD->iMax; i++) fprintf(fp, "%5lf ", pMEQ->q[i]);
        fprintf(fp, "\n#\n");
        
        fprintf(fp, "# matrix elements in [MEV fm^3] \n");
        gsl_matrix *U = gsl_matrix_alloc(pSVD->iMax+1, pSVD->nComponent);
        gsl_matrix *M = gsl_matrix_alloc(pSVD->iMax+1, pSVD->iMax+1);
        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->sgU[p], pSVD->sgS[p], 0.0, U);
        gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, U, pSVD->sgV[p], 0.0, M);
        
        for(i=0; i<pSVD->iMax+1; i++){
            for(j=0; j<pSVD->iMax+1; j++){
                fprintf(fp, "%9.6lf ", gsl_matrix_get(M,i,j));
            }
            fprintf(fp,"\n");
        }
        fprintf(fp, "#\n");
        
        fclose(fp);
        UTI_InfoMessage0("done\n\n");
        
        gsl_matrix_free(U);
        gsl_matrix_free(M);
    }
    
    //*** loop over coupled partial waves
    for(p=0; p<pSVD->cpCount; p++) {
        
        //***recompose matrix
        gsl_matrix *U = gsl_matrix_alloc((pSVD->iMax+1)*2, pSVD->nComponent*2);
        gsl_matrix *M = gsl_matrix_alloc((pSVD->iMax+1)*2, (pSVD->iMax+1)*2);
        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpU[p], pSVD->cpS[p], 0.0, U);
        gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, U, pSVD->cpV[p], 0.0, M);
        
        gsl_matrix **MP = (gsl_matrix **)malloc(sizeof(gsl_matrix *)*3);
        for (i=0; i<3; i++){
            MP[i] = gsl_matrix_alloc(pSVD->iMax+1, pSVD->iMax+1);
        }
        
        for (i=0; i<pSVD->iMax+1; i++){
            for (j=0; j<pSVD->iMax+1; j++){
                gsl_matrix_set(MP[0],i,j,gsl_matrix_get(M,i,j));
                gsl_matrix_set(MP[1],i,j,gsl_matrix_get(M,i,j+pSVD->iMax+1));
                gsl_matrix_set(MP[2],i,j,gsl_matrix_get(M,i+pSVD->iMax+1,j+pSVD->iMax+1));
            }
        }
        
        
        for(int pp=0; pp<3; pp++){
            if(PWB.NoCD) {
                sprintf(filename, "%s/%s_JLLST%i%i%i%i%i.txt", PAR.MEDir, MEID, PWB.J[pSVD->cpPc[3*p+pp]], PWB.L[pSVD->cpPc[3*p+pp]], PWB.LL[pSVD->cpPc[3*p+pp]], PWB.S[pSVD->cpPc[3*p+pp]], PWB.T[pSVD->cpPc[3*p+pp]]);
            }
            else {
                sprintf(filename, "%s/%s_JLLSTMT%i%i%i%i%i%+i.txt", PAR.MEDir, MEID, PWB.J[pSVD->cpPc[3*p+pp]], PWB.L[pSVD->cpPc[3*p+pp]], PWB.LL[pSVD->cpPc[3*p+pp]], PWB.S[pSVD->cpPc[3*p+pp]], PWB.T[pSVD->cpPc[3*p+pp]], PWB.MT[pSVD->cpPc[3*p+pp]]);
            }
            
            UTI_InfoMessage("MEQ_WriteFile", "writing '%s'... ", filename);
            fp = fopen(filename,"w");
            
            fprintf(fp, "# %s/%s \n", PROG_ID, PROG_VERSION);
            fprintf(fp, "# \n");
            fprintf(fp, "# momentum space matrix elements \n");
            fprintf(fp, "# partial wave J=%i, L=%i/%i, S=%i, T=%i, MT=%i \n", PWB.J[pSVD->cpPc[3*p+pp]], PWB.L[pSVD->cpPc[3*p+pp]], PWB.LL[pSVD->cpPc[3*p+pp]], PWB.S[pSVD->cpPc[3*p+pp]], PWB.T[pSVD->cpPc[3*p+pp]], PWB.MT[pSVD->cpPc[3*p+pp]]);
            fprintf(fp, "# \n");
            fprintf(fp, "# momentum grid [fm^-1]\n");
            for (i=0; i<=pSVD->iMax; i++) fprintf(fp, "%5lf ", pMEQ->q[i]);
            fprintf(fp, "\n#\n");
            
            fprintf(fp, "# matrix elements in [MEV fm^3] \n");
            
            
            
            for (i=0; i<pSVD->iMax+1; i++){
                for (j=0; j<pSVD->iMax+1; j++){
                    fprintf(fp, "%9.6lf ", gsl_matrix_get(MP[pp],i,j));
                }
                fprintf(fp,"\n");
            }
            fprintf(fp, "#\n");
            fclose(fp);
            
            UTI_InfoMessage0("done\n\n");
        }
        
        
        gsl_matrix_free(U);
        gsl_matrix_free(M);
        for (i=0; i<3; i++){
            gsl_matrix_free(MP[i]);
        }
        free(MP);
        
    }

    
    /*
    // print singular components for coupled partial waves
    for(p=0; p<pSVD->cpCount; p++) {
        sprintf(filename, "%s/Coupled Partial Wave %i.txt", PAR.MEDir, p+1);
        
        
        UTI_InfoMessage("MEQ_WriteFile", "writing '%s'... ", filename);
        fp = fopen(filename,"w");
        
        fprintf(fp, "# %s/%s \n", PROG_ID, PROG_VERSION);
        fprintf(fp, "# \n");
        fprintf(fp, "# momentum space matrix elements \n");
        
        fprintf(fp, "# partial wave J=%i, L=%i/%i, S=%i, T=%i, MT=%i \n", PWB.J[pSVD->cpPc[3*p]], PWB.L[pSVD->cpPc[3*p]], PWB.LL[pSVD->cpPc[3*p]], PWB.S[pSVD->cpPc[3*p]], PWB.T[pSVD->cpPc[3*p]], PWB.MT[pSVD->cpPc[3*p]]);
        fprintf(fp, "# partial wave J=%i, L=%i/%i, S=%i, T=%i, MT=%i \n", PWB.J[pSVD->cpPc[3*p+1]], PWB.L[pSVD->cpPc[3*p+1]], PWB.LL[pSVD->cpPc[3*p+1]], PWB.S[pSVD->cpPc[3*p+1]], PWB.T[pSVD->cpPc[3*p+1]], PWB.MT[pSVD->cpPc[3*p+1]]);
        fprintf(fp, "# partial wave J=%i, L=%i/%i, S=%i, T=%i, MT=%i \n", PWB.J[pSVD->cpPc[3*p+2]], PWB.L[pSVD->cpPc[3*p+2]], PWB.LL[pSVD->cpPc[3*p+2]], PWB.S[pSVD->cpPc[3*p+2]], PWB.T[pSVD->cpPc[3*p+2]], PWB.MT[pSVD->cpPc[3*p+2]]);
        
        fprintf(fp, "# \n");
        fprintf(fp, "# momentum grid [fm^-1]\n");
        for(i=0; i<=pMEQ->iMax; i++) fprintf(fp, "%5lf ", pMEQ->q[i]);
        fprintf(fp, "\n#\n");
        
        fprintf(fp, "# matrix elements in SVD[MEV fm^3] \n");
        fprintf(fp, "# left singular vectors U, with size %zu, %zu\n", pSVD->cpU[p]->size1, pSVD->cpU[p]->size2);
        for(i=0; i<pSVD->cpU[p]->size1; i++) {
            for(j=0; j<pSVD->cpU[p]->size2; j++) {
                fprintf(fp, "%9.6lf ", gsl_matrix_get(pSVD->cpU[p],i,j));
            }
            fprintf(fp, "\n");
        }
        fprintf(fp, "#\n");
        
        fprintf(fp, "# right singular vectors V, with size %zu, %zu\n", pSVD->cpV[p]->size1, pSVD->cpV[p]->size2);
        for(i=0; i<pSVD->cpV[p]->size1; i++) {
            for(j=0; j<pSVD->cpV[p]->size2; j++) {
                fprintf(fp, "%9.6lf ", gsl_matrix_get(pSVD->cpV[p],i,j));
            }
            fprintf(fp, "\n");
        }
        fprintf(fp, "#\n");
        
        fprintf(fp, "# singular values S, with size %zu, %zu\n", pSVD->cpS[p]->size1, pSVD->cpS[p]->size2);
        for(i=0; i<pSVD->cpS[p]->size1; i++) {
            for(j=0; j<pSVD->cpS[p]->size2; j++) {
                fprintf(fp, "%9.6lf ", gsl_matrix_get(pSVD->cpS[p],i,j));
            }
            fprintf(fp, "\n");
        }
        fprintf(fp, "#\n");
        
        fclose(fp);
        UTI_InfoMessage0("done\n\n");

    }
    */
    
    
    
    //test result
    
    int Dim = (pSVD->iMax+1)*2;
    gsl_matrix *UtimeS = gsl_matrix_alloc(Dim, pSVD->nComponent*2);
    gsl_matrix *Int    = gsl_matrix_alloc(Dim, Dim);
    gsl_matrix_set_zero(UtimeS);
    gsl_matrix_set_zero(Int);
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,pSVD->cpU[0],pSVD->cpS[0],0.0,UtimeS);
    gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,UtimeS,pSVD->cpV[0],0.0,Int);
        
    gsl_matrix_add(Int, pSVD->cpT);
    gsl_matrix_scale(Int, INVMU);
        
    gsl_vector_complex *eval = gsl_vector_complex_alloc(Dim);
    gsl_matrix_complex *evec = gsl_matrix_complex_alloc(Dim,Dim);
    gsl_eigen_nonsymmv_workspace *w = gsl_eigen_nonsymmv_alloc(Dim);
        
    gsl_eigen_nonsymmv(Int, eval, evec, w);
    gsl_eigen_nonsymmv_free(w);
        
    gsl_eigen_nonsymmv_sort(eval, evec,GSL_EIGEN_SORT_VAL_DESC);
    
    for (i=0; i<Dim; i++){
        gsl_complex eval_i =gsl_vector_complex_get(eval,i);
        printf("eigenvalue = %g + %gi\n",GSL_REAL(eval_i),GSL_IMAG(eval_i));
    }
        
        
    gsl_vector_complex_free(eval);
    gsl_matrix_complex_free(evec);
    gsl_matrix_free(UtimeS);
    gsl_matrix_free(Int);
    

    //free space
    gsl_matrix_free(pSVD->sgWeight);
    gsl_matrix_free(pSVD->cpWeight);
    free(pSVD->sgPc);
    free(pSVD->cpPc);
    gsl_matrix_free(pSVD->cpEta);
    gsl_matrix_free(pSVD->sgEta);
    gsl_matrix_free(pSVD->cpT);
    gsl_matrix_free(pSVD->sgT);
    for (i=0; i<pSVD->sgCount; i++){
        gsl_matrix_free(pSVD->sgU[i]);
        gsl_matrix_free(pSVD->sgV[i]);
        gsl_matrix_free(pSVD->sgS[i]);
    }
    for (i=0; i<pSVD->cpCount; i++){
        gsl_matrix_free(pSVD->cpU[i]);
        gsl_matrix_free(pSVD->cpV[i]);
        gsl_matrix_free(pSVD->cpS[i]);
    }
    free(pSVD->sgU);
    free(pSVD->sgV);
    free(pSVD->sgS);
    free(pSVD->cpU);
    free(pSVD->cpV);
    free(pSVD->cpS);
    
}

//**********************************************************************
//*** eof **************************************************************
//**********************************************************************
/*
pSVD->sglSVec       = (double ***) malloc((size_t)(Dim1*sizeof(double**)));
pSVD->sglSVec[0]    = (double **) malloc((size_t)(Dim1*Dim2*sizeof(double*)));
pSVD->sglSVec[0][0] = (double *) malloc((size_t)(Dim1*Dim2*Dim3*sizeof(double)));
pSVD->sgrSVec       = (double ***) malloc((size_t)(Dim1*sizeof(double**)));
pSVD->sgrSVec[0]    = (double **) malloc((size_t)(Dim1*Dim2*sizeof(double*)));
pSVD->sgrSVec[0][0] = (double *) malloc((size_t)(Dim1*Dim2*Dim3*sizeof(double)));
pSVD->sgSVal        = (double ***) malloc((size_t)(Dim1*sizeof(double**)));
pSVD->sgSVal[0]     = (double **) malloc((size_t)(Dim1*Dim3*sizeof(double*)));
pSVD->sgSVal[0][0]  = (double *) malloc((size_t)(Dim1*Dim3*Dim3*sizeof(double)));
*/


/*
    fprintf(fp, "# matrix elements in SVD[MEV fm^3] \n");
    fprintf(fp, "# left singular vectors U, with size %zu, %zu\n", pSVD->sgU[p]->size1, pSVD->sgU[p]->size2);
    for(i=0; i<pSVD->sgU[p]->size1; i++) {
        for(j=0; j<pSVD->sgU[p]->size2; j++) {
            fprintf(fp, "%9.6lf ", gsl_matrix_get(pSVD->sgU[p],i,j));
        }
        fprintf(fp, "\n");
    }
    fprintf(fp, "#\n");
    
    fprintf(fp, "# right singular vectors V, with size %zu, %zu\n", pSVD->sgV[p]->size1, pSVD->sgV[p]->size2);
    for(i=0; i<pSVD->sgV[p]->size1; i++) {
        for(j=0; j<pSVD->sgV[p]->size2; j++) {
            fprintf(fp, "%9.6lf ", gsl_matrix_get(pSVD->sgV[p],i,j));
        }
        fprintf(fp, "\n");
    }
    fprintf(fp, "#\n");
    
    fprintf(fp, "# singular values S, with size %zu, %zu\n", pSVD->sgS[p]->size1, pSVD->sgS[p]->size2);
    for(i=0; i<pSVD->sgS[p]->size1; i++) {
        for(j=0; j<pSVD->sgS[p]->size2; j++) {
            fprintf(fp, "%9.6lf ", gsl_matrix_get(pSVD->sgS[p],i,j));
        }
        fprintf(fp, "\n");
    }
    fprintf(fp, "#\n");
    
    fclose(fp);
    UTI_InfoMessage0("done\n\n");
}



for(p=0; p<pSVD->cpCount; p++) {
    sprintf(filename, "%s/Coupled Partial Wave %i.txt", PAR.MEDir, p+1);
    
    
    UTI_InfoMessage("MEQ_WriteFile", "writing '%s'... ", filename);
    fp = fopen(filename,"w");
    
    fprintf(fp, "# %s/%s \n", PROG_ID, PROG_VERSION);
    fprintf(fp, "# \n");
    fprintf(fp, "# momentum space matrix elements \n");
    
    fprintf(fp, "# partial wave J=%i, L=%i/%i, S=%i, T=%i, MT=%i \n", PWB.J[pSVD->cpPc[3*p]], PWB.L[pSVD->cpPc[3*p]], PWB.LL[pSVD->cpPc[3*p]], PWB.S[pSVD->cpPc[3*p]], PWB.T[pSVD->cpPc[3*p]], PWB.MT[pSVD->cpPc[3*p]]);
    fprintf(fp, "# partial wave J=%i, L=%i/%i, S=%i, T=%i, MT=%i \n", PWB.J[pSVD->cpPc[3*p+1]], PWB.L[pSVD->cpPc[3*p+1]], PWB.LL[pSVD->cpPc[3*p+1]], PWB.S[pSVD->cpPc[3*p+1]], PWB.T[pSVD->cpPc[3*p+1]], PWB.MT[pSVD->cpPc[3*p+1]]);
    fprintf(fp, "# partial wave J=%i, L=%i/%i, S=%i, T=%i, MT=%i \n", PWB.J[pSVD->cpPc[3*p+2]], PWB.L[pSVD->cpPc[3*p+2]], PWB.LL[pSVD->cpPc[3*p+2]], PWB.S[pSVD->cpPc[3*p+2]], PWB.T[pSVD->cpPc[3*p+2]], PWB.MT[pSVD->cpPc[3*p+2]]);
    
    fprintf(fp, "# \n");
    fprintf(fp, "# momentum grid [fm^-1]\n");
    for(i=0; i<=pMEQ->iMax; i++) fprintf(fp, "%5lf ", pMEQ->q[i]);
    fprintf(fp, "\n#\n");
    
    fprintf(fp, "# matrix elements in SVD[MEV fm^3] \n");
    fprintf(fp, "# left singular vectors U, with size %zu, %zu\n", pSVD->cpU[p]->size1, pSVD->cpU[p]->size2);
    for(i=0; i<pSVD->cpU[p]->size1; i++) {
        for(j=0; j<pSVD->cpU[p]->size2; j++) {
            fprintf(fp, "%9.6lf ", gsl_matrix_get(pSVD->cpU[p],i,j));
        }
        fprintf(fp, "\n");
    }
    fprintf(fp, "#\n");
    
    fprintf(fp, "# right singular vectors V, with size %zu, %zu\n", pSVD->cpV[p]->size1, pSVD->cpV[p]->size2);
    for(i=0; i<pSVD->cpV[p]->size1; i++) {
        for(j=0; j<pSVD->cpV[p]->size2; j++) {
            fprintf(fp, "%9.6lf ", gsl_matrix_get(pSVD->cpV[p],i,j));
        }
        fprintf(fp, "\n");
    }
    fprintf(fp, "#\n");
    
    fprintf(fp, "# singular values S, with size %zu, %zu\n", pSVD->cpS[p]->size1, pSVD->cpS[p]->size2);
    for(i=0; i<pSVD->cpS[p]->size1; i++) {
        for(j=0; j<pSVD->cpS[p]->size2; j++) {
            fprintf(fp, "%9.6lf ", gsl_matrix_get(pSVD->cpS[p],i,j));
        }
        fprintf(fp, "\n");
    }
    fprintf(fp, "#\n");
    
    fclose(fp);
    UTI_InfoMessage0("done\n\n");

}
*/

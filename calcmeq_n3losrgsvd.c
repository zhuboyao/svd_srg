//**********************************************************************
//*** calcmeq_n3losrgsvd.c
//*** 01/01/2020, B. Zhu
//**********************************************************************


#include "libMEQ/GLO_Base.h"
#include "libMEQ/PAR_Base.h"
#include "libMEQ/UTI_Base.h"
#include "libMEQ/PWB_Base.h"
#include "libMEQ/MEQ_Base.h"
#include "libMEQ/N3LO_CalcMEQ.h"
#include "libMEQ/SRG_Base.h"
#include "libMEQ/SVD_Base.h"
#include "libMEQ/SRGbySVD_Base.h"


//**********************************************************************
//*** global variables *************************************************
//**********************************************************************
//
//*** local parameters
struct {
  int      qType;
  double   qMax;
  int      iMax;
  double   sMax[2];
  int      MEFmt;
  int      nCmpnt;
} PARX;



//**********************************************************************
//*** parse commandline ************************************************
//**********************************************************************
//
void ParseCommandline(int argc, char **argv) 
{
  int   i;

  if(argc<2){   
    UTI_SubSectionMessage("Usage");

    UTI_Message("  %s [variable]=[value] ... [switch] ... \n\n", argv[0]);

    UTI_SubSectionMessage("Variables");

    UTI_Message("  LMax     : maximum L [8] \n\n");

    UTI_Message("  qType    : tpye of momentum grid [*0:equidist, 1:HermiteZero, 2:Hermite, 3:Laguerre] \n");
    UTI_Message("  qMax     : maximum relative momentum q \n");
    UTI_Message("  iMax     : maximum index of momentum grid \n");
    UTI_Message("  nCmp     : number of singular component \n\n");
    
    UTI_Message("  sMax     : number of steps for SRG evolution [0.01 or 0.01:0.01 (even:odd channels)] \n\n");
      

    UTI_Message("  MEID     : matrix element file ID [=IntID]\n");
    UTI_Message("  MEDir    : directory for matrix element files [./me] \n\n");

    UTI_SubSectionMessage("Switches");

    UTI_Message("  NoCD     : average over charge-dependence \n");
    UTI_Message("  \n\n");

    exit(1);
  }
   
  
  //*** set default parameter values
  PAR_Init();

  PARX.qType     = 0;
  PARX.qMax      = 7.0;
  PARX.iMax      = 70;
  PARX.sMax[0]   = 0.01;
  PARX.sMax[1]   = 0.01;
  PARX.MEFmt     = 1;
  PARX.nCmpnt    = PARX.iMax;

  strcpy(PAR.IntID, "n3lo");
 

  //*** parse commandline options
  for(i=1; i<=argc-1; i++){
     if(strstr(argv[i],"LMax=")!=NULL){
      sscanf(argv[i],"LMax=%i", &PAR.LMax); continue;}

    if(strstr(argv[i],"qType=")!=NULL){
      sscanf(argv[i],"qType=%i", &PARX.qType); continue;}
  
    if(strstr(argv[i],"qMax=")!=NULL){
      sscanf(argv[i],"qMax=%lf", &PARX.qMax); continue;}

    if(strstr(argv[i],"iMax=")!=NULL){
      sscanf(argv[i],"iMax=%i", &PARX.iMax); continue;}
    
    if(strstr(argv[i],"nCmp=")!=NULL){
      sscanf(argv[i],"nCmp=%i", &PARX.nCmpnt); continue;}
    
    if(strstr(argv[i],"sMax=")!=NULL){
      if(strstr(argv[i],":")!=NULL){
        sscanf(argv[i],"sMax=%lf:%lf", &PARX.sMax[0], &PARX.sMax[1]);
      } else {
        sscanf(argv[i],"sMax=%lf", &PARX.sMax[0]);
        PARX.sMax[1] = PARX.sMax[0];
      }
      continue;
    }
  
      
    if(strstr(argv[i],"MEFmt=")!=NULL){
      sscanf(argv[i],"MEFmt=%i", &PARX.MEFmt); continue;}
  
    if(strstr(argv[i],"MEID=")!=NULL){
      sscanf(argv[i],"MEID=%s", PAR.MEID); continue;}
 
    if(strstr(argv[i],"MEDir=")!=NULL){
      sscanf(argv[i],"MEDir=%s", PAR.MEDir); continue;}  

    if(strstr(argv[i],"NoCD")!=NULL){
      PAR.NoCD=1; continue;}
  }

  //*** derived quantities
  PAR_Update();
   
  if(strlen(PAR.MEID)==0) {
    if(PARX.sMax[0]==PARX.sMax[1]) {
      sprintf(PAR.MEID, "%s_srg%04i", PAR.IntID, (int)(10000*PARX.sMax[0])); 
    } else {
      sprintf(PAR.MEID, "%s_srg%04i.%04i", PAR.IntID, (int)(10000*PARX.sMax[0]), (int)(10000*PARX.sMax[1])); 
    }
  }  
    
  //*** print parameters
  UTI_Message("       LMax = %i \n", PAR.LMax);
  UTI_Message("       NoCD = %i \n\n", PAR.NoCD);

  UTI_Message("      qType = %i \n", PARX.qType);
  UTI_Message("       qMax = %g \n", PARX.qMax);
  UTI_Message("       iMax = %i \n", PARX.iMax);
  UTI_Message("       nCmp = %i \n\n", PARX.nCmpnt);

  UTI_Message("       sMax = %g:%g \n\n", PARX.sMax[0], PARX.sMax[1]);
    
  UTI_Message("      IntID = %s \n", PAR.IntID);
  UTI_Message("       MEID = %s \n", PAR.MEID);  
  UTI_Message("      MEFmt = %i \n", PARX.MEFmt); 
  UTI_Message("      MEDir = %s \n\n", PAR.MEDir);
}


//**********************************************************************
//*** main *************************************************************
//**********************************************************************
//
int main(int argc, char **argv) 
{
  MEQ_Struct   MEQ;
  SVD_Struct   SVD;
 
  UTI_HeaderMessage(__FILE__, __DATE__, __TIME__);

  UTI_SectionMessage("Parameters");
 
  ParseCommandline(argc, argv);
     
  
  //**** initialization **************************************************
  UTI_SectionMessage("Initialization");

  PWB_Init(PAR.LMax, PAR.NoCD);

  MEQ_Init(&MEQ, PARX.qType, PARX.qMax, PARX.iMax);
    
  SVD_Init(&SVD, PARX.iMax, PARX.nCmpnt);
    
  //***** compute Q matrix elements *************************************
  UTI_SectionMessage("Compute Q Matrix Elements");

  N3LO_CalcMEQ(&MEQ);
    
  SVD_MEQ(&MEQ, &SVD);
  
  SRGbySVD_EvolveMEQ(&SVD, PARX.sMax);
    
  //***** write matrix elements *****************************************
  UTI_SectionMessage("Write Matrix Elements");

  SVD_WriteFile(&MEQ, &SVD, PAR.MEID);
  
  UTI_FooterMessage();

  return(0);
}

//**********************************************************************
//*** eof **************************************************************
//**********************************************************************

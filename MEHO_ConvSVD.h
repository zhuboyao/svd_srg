//
//  MEHO_ConvSVD.h
//  
//
//  Created by Boyao Zhu on 1/6/20.
//

#ifndef MEHO_ConvSVD_h
#define MEHO_ConvSVD_h

#include "GLO_Base.h"
#include "PAR_Base.h"
#include "PWB_Base.h"
#include "BAS_WaveFunc.h"
#include "MEQ_Base.h"
//#include "MEHO_Base.h"


//**********************************************************************
//*** data structures **************************************************
//**********************************************************************
//
typedef struct {
  METype_Enum    METype;
  int            EMax;
  int            NMax;
  int            LMax;
  int            NoCD;
  enlTrunc_Enum  ENLTrunc;
  // matrix element data
  gsl_matrix     **sgU;         // 3-dim array of matrix elements for partial waves
  gsl_matrix     **sgV;
  gsl_matrix     **sgS;
  gsl_matrix     **cpU;
  gsl_matrix     **cpV;
  gsl_matrix     **cpS;
} MEHO_Struct;



//**********************************************************************
//*** routines *********************************************************
//**********************************************************************
//
void MEHO_Init(MEHO_Struct *pMEHO, SVD_Struct *pSVD, METype_Enum METype, int EMax, int NMax, int LMax, int NoCD);

void MEHO_ConvertSVD(MEHO_Struct *pMEHO, SVD_Struct *pSVD);

#endif /* MEHO_ConvSVD_h */

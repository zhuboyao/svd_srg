//
//  MEHO_ConvSVD.c
//  
//
//  Created by Boyao Zhu on 1/6/20.
//

#include "MEHO_ConvSVD.h"

//**********************************************************************
//*** compute MEHO by transforming MEQ *********************************
//**********************************************************************
//
void MEHO_Init(MEHO_Struct *pMEHO, SVD_Struct *pSVD, METype_Enum METype, int EMax, int NMax, int LMax, int NoCD)
{
    int i,j,Dim1,Dim2,Dim3;
    
    // maximum quantum numbers and truncation scheme
    pMEHO->METype   = METype;
    pMEHO->EMax     = EMax;
    pMEHO->NMax     = NMax;
    pMEHO->LMax     = LMax;
    pMEHO->NoCD     = NoCD;
    pMEHO->ENLTrunc = enlTruncNone;
    
    if(EMax!=0 && NMax!=0 && LMax!=0){
        pMEHO ->ENLTrunc = enlTRUncENL;
    }
    else if(EMax!=0 && NMax==0 && LMax!=0 && LMax!=EMax){
        pMEHO->ENLTrunc = enlTruncENL;
        pMEHO->NMax     = (int)(EMax/2);
    }
    else if (EMax==0 && NMax!=0 && LMax!=0){
        pMEHO->ENLTrunc = enlTruncNL;
        pMEHO->EMax     = 2*NMax+LMax;
    }
    else if(EMax!=0 && NMax==0 && (LMax==0 || LMax==EMax)){
        pMEHO->ENLTrunc = enlTruncE;
        pMEHO->NMax     = (int)(EMax/2);
        pMEHO->LMax     = EMax;
    }
    
    if(pMEHO->LMax!=PWB.LMax) {
        UTI_ErrorMessage("MEHO_Init", "LMax=%i inconsistent with PWB.LMax=%i! \n", pMEHO->LMax, PWB.LMax);
        exit(-1);
    }
    // allocate pw matrix-element array
    Dim1 = pSVD->sgCount;
    Dim2 = pMEHO->NMax+1;
    Dim3 = pSVD->nComponent;
    
    pMEHO->sgU          = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
    for (i=0; i<Dim1; i++)
        pMEHO->sgU[i]   = (gsl_matrix *) gsl_matrix_alloc(Dim2, Dim3);
    
    pMEHO->sgV          = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
    for (i=0; i<Dim1; i++)
        pMEHO->sgV[i]   = (gsl_matrix *) gsl_matrix_alloc(Dim2, Dim3);
    
    pMEHO->sgS          = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
    for (i=0; i<Dim1; i++)
        pMEHO->sgS[i]   = (gsl_matrix *) gsl_matrix_alloc(Dim3, Dim3);
    
    if(!pMEHO->sgU || !pMEHO->sgU[0]){
        UTI_ErrorMessage("MEHO_Init", "could not allocate MEHO structure for single partial wave left singular vectors!\n");
        exit(-1);
    }
    
    if(!pMEHO->sgV || !pMEHO->sgV[0]){
        UTI_ErrorMessage("MEHO_Init", "could not allocate MEHO structure for single partial wave right singular vectors!\n");
        exit(-1);
    }
    
    if(!pMEHO->sgS || !pMEHO->sgS[0]){
        UTI_ErrorMessage("MEHO_Init", "could not allocate MEHO structure for single partial wave singular values!\n");
        exit(-1);
    }
    
    if(pSVD->cpCount != 0){
        // allocate memory for coupled partial wave singular vectors and singular values
        Dim1 = pSVD->cpCount;
        Dim2 = (pMEHO->NMax+1)*2;
        Dim3 = pSVD->nComponent*2;
        
        pMEHO->cpU          = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
        for (i=0; i<Dim1; i++)
            pMEHO->cpU[i]   = (gsl_matrix *) gsl_matrix_alloc(Dim2, Dim3);
        
        pMEHO->cpV          = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
        for (i=0; i<Dim1; i++)
            pMEHO->cpV[i]   = (gsl_matrix *) gsl_matrix_alloc(Dim2, Dim3);
        
        pMEHO->cpS          = (gsl_matrix **) malloc(sizeof(gsl_matrix *)*Dim1);
        for (i=0; i<Dim1; i++)
            pMEHO->cpS[i]   = (gsl_matrix *) gsl_matrix_alloc(Dim3, Dim3);
        
        if(!pMEHO->cpU || !pMEHO->cpU[0]){
            UTI_ErrorMessage("MEHO_Init", "could not allocate MEHO structure for coupled partial wave left singular vectors!\n");
            exit(-1);
        }
        
        if(!pMEHO->cpV || !pMEHO->cpV[0]){
            UTI_ErrorMessage("MEHO_Init", "could not allocate MEHO structure for coupled partial wave right singular vectors!\n");
            exit(-1);
        }
        if(!pMEHO->cpS || !pMEHO->cpS[0]){
            UTI_ErrorMessage("MEHO_Init", "could not allocate MEHO structure for coupled partial wave singular values!\n");
            exit(-1);
        }
    }
}



void MEHO_ConvertSVD(MEHO_Struct *pMEHO, SVD_Struct *pSVD)
{
    int i,j,p;
    int iMax,nComponent,NMax;
    double BAS;
    
    iMax = pSVD->iMax;
    nComponent = pSVD->nComponent;
    NMax = pMEHO->NMax;
    
    UTI_InfoMessage("MEHO_ConvertSVD", "transforming singular components of momentum space U's, S's, V's to oscillator basis... \n");
    UTI_InfoMessage("MEHO_ConvertSVD", "[");
    
    //build transformation matrix for single partial wave
    for(p=0; p<pSVD->sgCount; p++) {
        gsl_matrix *Mom_HOsg = gsl_matrix_alloc(NMax+1, iMax+1);
        //build matrix for each single matrix
        for(i=0; i<NMax+1; i++) {
            for(j=0; j<iMax+1; j++) {
                BAS = BAS_MomSpaceWaveFuncHO_RNL(i, PWB.L[pSVD->sgPc[p]], pMEQ->q[j]);
                gsl_matrix_set(Mom_HOsg, i, j, BAS);
            }
        }
        // do transformation
        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Mom_HOsg, pSVD->sgU[p], 0.0, pMEHO->sgU[p]);
        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Mom_HOsg, pSVD->sgV[p], 0.0, pMEHO->sgV[p]);
        gsl_matrix_memcpy(pMEHO->sgS[p], pSVD->sgS[p]);

        // free memory
        gsl_matrix_free(Mom_HOsg);
    }
    
    //build transformaation matrix for coupled partial wave
    for(p=0; p<pSVD->cpCount; p++) {
        gsl_matrix *Mom_HOcp = gsl_matrix_alloc((NMax+1)*2, (iMax+1)*2);
        //build matrix for each coupled matrix
        for(i=0; i<NMax+1; i++) {
            for(j=0; j<iMax+1; j++) {
                BAS = BAS_MomSpaceWaveFuncHO_RNL(i, PWB.L[pSVD->cpPc[3*p+0]], pMEQ->q[j]);
                gsl_matrix_set(Mom_HOcp,i,j,BAS);
                BAS = BAS_MomSpaceWaveFuncHO_RNL(i, PWB.L[pSVD->cpPc[3*p+2]], pMEQ->q[j]);
                gsl_matrix_set(Mom_HOcp,NMax+1+i, iMax+1+j,BAS);
                BAS = BAS_MomSpaceWaveFuncHO_RNL(i, PWB.L[pSVD->cpPc[3*p+1]], pMEQ->q[j]);
                gsl_matrix_set(Mom_HOcp,i, iMax+1+j, BAS);
                gsl_matrix_set(Mom_HOcp,iMax+1+i, j, BAS);
                }
        }
        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Mom_HOcp, pSVD->cpU[p], 0.0, pMEHO->cpU[p]);
        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Mom_HOcp, pSVD->cpV[p], 0.0, pMEHO->cpV[p]);
        gsl_matrix_memcpy(pMEHO->cpS[p], pSVD->cpS[p]);
        
        //free memory
        gsl_matrix_free(Mom_HOcp);
    }
    
}

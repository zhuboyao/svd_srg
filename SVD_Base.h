//
//  SVD_Base.h
//  
//
//  Created by Boyao Zhu on 10/29/19.
//

#ifndef SVD_Base_h
#define SVD_Base_h

#include <stdio.h>
#include "GLO_Base.h"
#include "PAR_Base.h"
#include "UTI_Base.h"
#include "PWB_Base.h"
#include "MEQ_Base.h"
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <math.h>
#include <gsl/gsl_eigen.h>


//**********************************************************************
//*** data structures **************************************************
//**********************************************************************
//store singular vectors and singular values
typedef struct{
    int           nComponent;         // number of component
    int           iMax;               // maximum index of grid points
    gsl_matrix    **sgU;              // left singular vector for single pw
    gsl_matrix    **sgV;              // right singular vector for single pw
    gsl_matrix    **sgS;              // singular value for single pw
    gsl_matrix    **cpU;              // left singular vector for coupled pw
    gsl_matrix    **cpV;              // right singular vector for coupled pw
    gsl_matrix    **cpS;              // singular value for couple pw
    gsl_matrix    *sgT;               // T matrix in diagonal for single PW
    gsl_matrix    *cpT;               // for couple PW
    gsl_matrix    *sgEta;             // commutator of T and V in single PW
    gsl_matrix    *cpEta;             // commutator of TT and V in couple PW
    int           *sgPc;              // specify single partial waves
    int           *cpPc;              // specify couple partial waves
    int           sgCount;            // count number of single partial waves
    int           cpCount;            // count number of coupled partial waves
    gsl_matrix    *sgWeight;          // qWeight conversion matrix for single pw
    gsl_matrix    *cpWeight;          // qWeight conversion matrix for couple pw
    gsl_matrix    *tmpcpS;
    //gsl_matrix    *tmpsgS;
} SVD_Struct;
 


//**********************************************************************
//*** routines *********************************************************
//**********************************************************************
//
void SVD_Init(SVD_Struct *pSVD, int iMax, int nComponent);

void SVD_SinglePW(MEQ_Struct *pMEQ, SVD_Struct *pSVD, int p00, int count);

void SVD_CouplePW(MEQ_Struct *pMEQ, SVD_Struct *pSVD, int p00, int p02, int p22, int count);

void SVD_MEQ(MEQ_Struct *pMEQ, SVD_Struct *pSVD);

void SVD_WriteFile(MEQ_Struct *pMEQ, SVD_Struct *pSVD, char *MEID);

#endif /* SVD_Base_h */

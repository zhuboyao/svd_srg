//
//  SRGbySVD_Base.h
//  
//
//  Created by Boyao Zhu on 11/9/19.
//

#ifndef SRGbySVD_Base_h
#define SRGbySVD_Base_h


#include "GLO_Base.h"
#include "PAR_Base.h"
#include "UTI_Base.h"
#include "PWB_Base.h"
#include "MEQ_Base.h"
#include "SVD_Base.h"
#include <gsl/gsl_blas.h>
//#include <gsl/gsl_cblas.h>

//**********************************************************************
//*** routines *********************************************************
//**********************************************************************
//
void SRGbySVD_Evolve_SinglePW(SVD_Struct *pSVD, int p00, double sMax);

void SRGbySVD_Evolve_CouplePW(SVD_Struct *pMEQ, int p00, double sMax);

void SRGbySVD_EvolveMEQ(SVD_Struct *pSVD, double *sMax);

int SRG_RightHandSide_SinglePW(double s, const double V[], double dVds[], void *params);

int SRG_RightHandSide_CouplePW(double s, const double V[], double dVds[], void *params);

void get_Interaction(SVD_Struct *pSVD, gsl_matrix *U, gsl_matrix *S, gsl_matrix *V, gsl_matrix *Int);

void get_Eta(SVD_Struct *pSVD, gsl_matrix *Int);

void get_RHS(SVD_Struct *pSVD, gsl_matrix *tmpU, gsl_matrix *tmpS, gsl_matrix *tmpV, gsl_matrix *dU, gsl_matrix *dV, gsl_matrix *dS);
#endif /* SRGbySVD_Base_h */

//
//  SRGbySVD_Base.c
//  
//
//  Created by Boyao Zhu on 11/9/19.
//
//  Solving SRG evolution in momentum space via Runge-Kutta
//
//   (1/m)^-2 d/ds Vqq' =
//   d/dS Vqq' =
//      - (q^2 - q'^2)^2  Vqq'
//      + (1/m)^-1 \int dQ Q^2 (q^2 + q'^2 - 2 Q^2) VqQ VQq'
//
//   Vqq'           : standard matrix elements in units of [MeV fm^3]
//                    using 2/Pi normalization of spherical Bessels
//   1/m = INVMU    : inverse mass [MeV fm^2]
//   S = (1/m)^2 s  : flow parameter [fm^4]
//
//
#include "SRGbySVD_Base.h"


void get_Interaction(SVD_Struct *pSVD, gsl_matrix *U, gsl_matrix *S, gsl_matrix *V, gsl_matrix *Int){
    gsl_matrix *US = gsl_matrix_alloc((pSVD->iMax+1)*2, pSVD->nComponent*2);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, U, S, 0.0, US);
    gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, US, V, 0.0, Int);
    gsl_matrix_free(US);
}


void get_Eta(SVD_Struct *pSVD, gsl_matrix *Int){
    gsl_matrix *V_T = gsl_matrix_alloc((pSVD->iMax+1)*2,(pSVD->iMax+1)*2);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpT, Int, 0.0, pSVD->cpEta);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Int, pSVD->cpT, 0.0, V_T);
    gsl_matrix_sub(pSVD->cpEta, V_T);
    gsl_matrix_free(V_T);
}


void get_RHS(SVD_Struct *pSVD, gsl_matrix *tmpU, gsl_matrix *tmpS, gsl_matrix *tmpV, gsl_matrix *dU, gsl_matrix *dV, gsl_matrix *dS){
    
    gsl_matrix *EtaT = gsl_matrix_alloc((pSVD->iMax+1)*2, (pSVD->iMax+1)*2);
    gsl_matrix *TEta = gsl_matrix_alloc((pSVD->iMax+1)*2, (pSVD->iMax+1)*2);
    gsl_matrix *UEtaT = gsl_matrix_alloc(pSVD->nComponent*2, (pSVD->iMax+1)*2);
    
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpEta, pSVD->cpT, 0.0, EtaT);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpT, pSVD->cpEta, 0.0, TEta);
    gsl_matrix_sub(EtaT, TEta);
    
    gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, tmpU, EtaT, 0.0, UEtaT);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, UEtaT, tmpV, 0.0, dS);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpEta, tmpU, 0.0, dU);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpEta, tmpV, 0.0, dV);
    
    
    gsl_matrix_free(EtaT);
    gsl_matrix_free(TEta);
    gsl_matrix_free(UEtaT);
    
}


//**********************************************************************
//*** SRG evolution step for non-coupled channel ***********************
//**********************************************************************
//
//*** r.h.s. of the evolution equation
//
int SRG_RightHandSide_SinglePW(double s, const double V[], double dVds[], void *params)
{
    SVD_Struct *pSVD;
    int i, j;
    int iDim, nComponent;
    
    pSVD = (SVD_Struct *)params;
    iDim = pSVD->iMax+1;
    nComponent = pSVD->nComponent;
    
    //temperary matrices for singular components
    gsl_matrix *tmpU   = gsl_matrix_alloc(iDim, nComponent);
    gsl_matrix *tmpV   = gsl_matrix_alloc(iDim, nComponent);
    gsl_matrix *tmpS   = gsl_matrix_alloc(nComponent, nComponent);
    gsl_matrix *tmpInt = gsl_matrix_alloc(iDim, iDim);
    gsl_matrix *tmp    = gsl_matrix_alloc(iDim, iDim);
    gsl_matrix *tmps   = gsl_matrix_alloc(nComponent, iDim);
    gsl_matrix *tmpUS  = gsl_matrix_alloc(iDim, nComponent);
    
    
    //transfer matrix elements from tmp array
    for(i=0; i<iDim; i++) {
        for(j=0; j<nComponent; j++) {
            gsl_matrix_set(tmpU, i, j, V[i+j*iDim]);
            gsl_matrix_set(tmpV, i, j, V[nComponent*iDim+i+j*iDim]);
        }
    }
    
    for(i=0; i<nComponent; i++) {
        for(j=0; j<nComponent; j++) {
            gsl_matrix_set(tmpS, i, j, V[nComponent*iDim*2+i+j*nComponent]);
        }
    }
    
    //compute U*S
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, tmpU, tmpS, 0.0, tmpUS);

    
    //get Full interaction
    gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, tmpUS, tmpV, 0.0, tmpInt);
    
    
    //get Eta
    gsl_matrix_set_zero(pSVD->sgEta);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->sgT, tmpInt, 0.0, pSVD->sgEta);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, tmpInt, pSVD->sgT, 0.0, tmp);
    gsl_matrix_sub(pSVD->sgEta, tmp);
    
    //get right hand side
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->sgEta, pSVD->sgT, 0.0, tmp);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->sgT, pSVD->sgEta, 0.0, tmpInt);
    gsl_matrix_sub(tmp, tmpInt);
    //gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, tmpU, tmp, 0.0, tmps);
    
    //gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, tmps, tmpV, 0.0, tmpS);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->sgEta, tmpU, 0.0, tmpU);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->sgEta, tmpV, 0.0, tmpV);
    gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, tmpU, tmp, 0.0, tmps);
    
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, tmps, tmpV, 0.0, tmpS);
    
    for(i=0; i<iDim; i++) {
        for(j=0; j<nComponent; j++) {
            dVds[i+j*iDim] = gsl_matrix_get(tmpU,i,j);
            dVds[nComponent*iDim+i+j*iDim] = gsl_matrix_get(tmpV,i,j);
        }
    }
    
    for(i=0; i<nComponent; i++) {
        for(j=0; j<nComponent; j++) {
            dVds[nComponent*iDim*2+i+j*nComponent] = gsl_matrix_get(tmpS,i,j);
        }
    }
    
    gsl_matrix_free(tmpU);
    gsl_matrix_free(tmpV);
    gsl_matrix_free(tmpS);
    gsl_matrix_free(tmpInt);
    gsl_matrix_free(tmp);
    gsl_matrix_free(tmps);
    gsl_matrix_free(tmpUS);
    
    return (GSL_SUCCESS);
}


//*** solve evaluation equation using adaptive gsl routine
//
void SRGbySVD_Evolve_SinglePW(SVD_Struct *pSVD, int p00, double sMax)
{
    double *V;
    double sDel, s;
    int    i, j, iDim, nComponent, Dim;
    int    stat;
    
    //allocate temporary arrays
    iDim = pSVD->iMax+1;
    nComponent = pSVD->nComponent;
    Dim = iDim*nComponent*2+nComponent*nComponent;
    
    V    = (double *)malloc(Dim*sizeof(double));
    
    //transfer matrix elements to tmp vector
    //transfor singular vectors
    for(i=0; i<iDim; i++) {
        for(j=0; j<nComponent; j++) {
            V[i+j*iDim] = gsl_matrix_get(pSVD->sgU[p00],i,j);
            V[nComponent*iDim+i+j*iDim] = gsl_matrix_get(pSVD->sgV[p00],i,j);
        }
    }
    
    //transfer singular values
    for(i=0; i<nComponent; i++) {
        for(j=0; j<nComponent; j++) {
            V[nComponent*iDim*2+i+j*nComponent] = gsl_matrix_get(pSVD->sgS[p00],i,j);
        }
    }
    
    //initialize solver
    const gsl_odeiv_step_type * ode_step_type = gsl_odeiv_step_rkf45;
    gsl_odeiv_step * ode_step = gsl_odeiv_step_alloc(ode_step_type, Dim);
    
    gsl_odeiv_control * ode_control = gsl_odeiv_control_y_new(1e-6, 0.0);
    gsl_odeiv_evolve *  ode_evolve = gsl_odeiv_evolve_alloc(Dim);
    
    gsl_odeiv_system ode_system = {SRG_RightHandSide_SinglePW, NULL, Dim, pSVD};
    
    // call solver
    s    = 0.0;
    sDel = 1e-4;
    
    while (s<sMax) {
        stat = gsl_odeiv_evolve_apply(ode_evolve, ode_control, ode_step, &ode_system, &s, sMax, &sDel, V);
        
        if (stat!=GSL_SUCCESS) {
            UTI_ErrorMessage("SRGbySVD_Evolve_SinglePW", "problem appeared in gsl_odeiv_evolve_apply!\n\n");
            exit(-1);
        }
    }
    
    //transfer matrix elements from tmp array
    for(i=0; i<iDim; i++) {
        for(j=0; j<nComponent; j++) {
            gsl_matrix_set(pSVD->sgU[p00], i, j, V[i+j*iDim]);
            gsl_matrix_set(pSVD->sgV[p00], i, j, V[nComponent*iDim+i+j*iDim]);
        }
    }
    
    for(i=0; i<nComponent; i++) {
        for(j=0; j<nComponent; j++) {
            gsl_matrix_set(pSVD->sgS[p00], i, j, V[nComponent*iDim*2+i+j*nComponent]);
        }
    }
    
    // clean up
    gsl_odeiv_evolve_free(ode_evolve);
    gsl_odeiv_control_free(ode_control);
    gsl_odeiv_step_free(ode_step);
    free(V);
}



//**********************************************************************
//*** SRG evolution step for coupled channel ***************************
//**********************************************************************
//
//*** r.h.s. of the evolution equiation
//

int SRG_RightHandSide_CouplePW(double s, const double V[], double dVds[], void *params)
{
    SVD_Struct *pSVD;
    int        i, j, iDim, nComponent;
    
    pSVD = (SVD_Struct *)params;
    iDim = (pSVD->iMax+1)*2;
    nComponent = pSVD->nComponent*2;
    
    //temperary matrices for singular components
    gsl_matrix *tmpU   = gsl_matrix_alloc(iDim, nComponent);
    gsl_matrix *tmpV   = gsl_matrix_alloc(iDim, nComponent);
    gsl_matrix *tmpS   = gsl_matrix_alloc(nComponent, nComponent);
    gsl_matrix *tmpInt = gsl_matrix_alloc(iDim, iDim);
    gsl_matrix *dU = gsl_matrix_alloc(iDim, nComponent);
    gsl_matrix *dV = gsl_matrix_alloc(iDim, nComponent);
    
    //transfer matrix elements from tmp array
    for(i=0; i<iDim; i++) {
        for(j=0; j<nComponent; j++) {
            gsl_matrix_set(tmpU, i, j, V[i+j*iDim]);
            gsl_matrix_set(tmpV, i, j, V[nComponent*iDim+i+j*iDim]);
        }
    }
    
    
    // get interaction
    gsl_matrix *US = gsl_matrix_alloc(iDim, nComponent);
    gsl_matrix *EtaT = gsl_matrix_alloc(iDim, iDim);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpT, tmpV, 0.0, US);
    gsl_blas_dgemm(CblasTrans, CblasNoTrans, -1.0, tmpU, US, 0.0, tmpS);
    gsl_matrix_add(tmpS, pSVD->tmpcpS);
    
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, tmpU, tmpS, 0.0, US);
    gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, US, tmpV, 0.0, tmpInt);
    // get Eta
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpT, tmpInt, 0.0, pSVD->cpEta);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, tmpInt, pSVD->cpT, 0.0, EtaT);
    gsl_matrix_sub(pSVD->cpEta, EtaT);
    
    
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpEta, tmpU, 0.0, dU);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpEta, tmpV, 0.0, dV);
    

    for(i=0; i<iDim; i++) {
        for(j=0; j<nComponent; j++) {
            dVds[i+j*iDim] = gsl_matrix_get(dU,i,j);
            dVds[nComponent*iDim+i+j*iDim] = gsl_matrix_get(dV,i,j);
        }
    }
    
    gsl_matrix_free(EtaT);
    gsl_matrix_free(US);
    gsl_matrix_free(tmpU);
    gsl_matrix_free(tmpV);
    gsl_matrix_free(tmpS);
    gsl_matrix_free(tmpInt);
    gsl_matrix_free(dU);
    gsl_matrix_free(dV);
    
    return (GSL_SUCCESS);
}


//*** solve evaluation equation using adaptive gsl routine
//
void SRGbySVD_Evolve_CouplePW(SVD_Struct *pSVD, int p00, double sMax)
{
    double *V;
    double sDel, s;
    int    i, j, iDim, nComponent, Dim;
    int    stat;
    
    //allocate temporary arrays
    iDim = (pSVD->iMax+1)*2;
    nComponent = pSVD->nComponent*2;
    Dim = iDim*nComponent*2;
    
    V = (double *)malloc(Dim*sizeof(double));
    
    //transfer matrix elements to tmp vector
    //transfer singular vectors
    for(i=0; i<iDim; i++) {
        for (j=0; j<nComponent; j++) {
            V[i+j*iDim] = gsl_matrix_get(pSVD->cpU[p00],i,j);
            V[nComponent*iDim+i+j*iDim] = gsl_matrix_get(pSVD->cpV[p00],i,j);
        }
    }
    
    gsl_matrix *tmp = gsl_matrix_alloc(nComponent,nComponent);
    gsl_matrix *TV = gsl_matrix_alloc(iDim, nComponent);
    pSVD->tmpcpS = gsl_matrix_alloc(nComponent, nComponent);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpT, pSVD->cpV[p00], 0.0, TV);
    gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, pSVD->cpU[p00], TV,0.0, tmp);
    gsl_matrix_add(pSVD->cpS[p00],tmp);
    gsl_matrix_memcpy(pSVD->tmpcpS, pSVD->cpS[p00]);
    
    
    //initialize solver
    const gsl_odeiv_step_type * ode_step_type = gsl_odeiv_step_rkf45;
    gsl_odeiv_step * ode_step = gsl_odeiv_step_alloc(ode_step_type, Dim);
    
    gsl_odeiv_control * ode_control = gsl_odeiv_control_y_new(1e-6, 0.0);
    gsl_odeiv_evolve * ode_evolve = gsl_odeiv_evolve_alloc(Dim);
    
    gsl_odeiv_system ode_system = {SRG_RightHandSide_CouplePW, NULL, Dim, pSVD};
    
    //call solver
    s    = 0.0;
    sDel = 1e-4;
    
    while(s<sMax) {
        stat = gsl_odeiv_evolve_apply(ode_evolve, ode_control, ode_step, &ode_system, &s, sMax, &sDel, V);
        if(stat!=GSL_SUCCESS) {
            UTI_ErrorMessage("SRG_Evolve_CouplePW", "problem appeared in gsl_odeiv_evolve_apply!\n\n");
            exit(-1);
        }
    }
    
    //transfer matrix elements from tmp array
    for(i=0; i<iDim; i++) {
        for(j=0; j<nComponent; j++) {
            gsl_matrix_set(pSVD->cpU[p00], i, j, V[i+j*iDim]);
            gsl_matrix_set(pSVD->cpV[p00], i, j, V[nComponent*iDim+i+j*iDim]);
        }
    }
    
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, pSVD->cpT, pSVD->cpV[p00], 0.0, TV);
    gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, pSVD->cpU[p00], TV,0.0, tmp);
    gsl_matrix_sub(pSVD->cpS[p00],tmp);
    gsl_matrix_free(pSVD->tmpcpS);
    gsl_matrix_free(tmp);
    gsl_matrix_free(TV);
    
    
    //clean up
    gsl_odeiv_evolve_free(ode_evolve);
    gsl_odeiv_control_free(ode_control);
    gsl_odeiv_step_free(ode_step);
    free(V);
}


 
//**********************************************************************
//*** correlated matrix elements ***************************************
//**********************************************************************
//
void SRGbySVD_EvolveMEQ(SVD_Struct *pSVD, double *sMax)
{
    int p00, p02, p22, pp;
    
    UTI_InfoMessage("SRGbySVD_EvolveMEQ", "starting SRG evolution (sMax=%g:%g)...\n", sMax[0], sMax[1]);
    UTI_InfoMessage("SRGbySVD_EvolveMEQ", "[");
    
    //*** loop over single partial waves
    
    // OpenMP parallelization for following loop
    /*
    #pragma omp parallel for default(shared) \
                private(p00) \
                schedule(dynamic,1)
    */
    for(p00=0; p00<(pSVD->sgCount); p00++) {
        UTI_InfoMessage0("+");
        SRGbySVD_Evolve_SinglePW(pSVD, p00, sMax[PWB.L[pSVD->sgPc[p00]]%2]);
    }
    
    /*
    #pragma omp parallel for default(shared) \
                private(p00) \
                schedule(dynamic,1)
    */
    for(p00=0; p00<pSVD->cpCount; p00++) {
        UTI_InfoMessage0("#");
        SRGbySVD_Evolve_CouplePW(pSVD, p00, sMax[PWB.L[pSVD->cpPc[p00]]%2]);
    }
    
    UTI_InfoMessage0("]\n\n");
    
}

//**********************************************************************
//*** eof **************************************************************
//**********************************************************************
